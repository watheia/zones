import { LearnMoreLink, LearnMoreLinkProps } from "./learn-more-link"

export { LearnMoreLink, LearnMoreLinkProps }
export default LearnMoreLink

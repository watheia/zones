import React, { useState } from "react"
import { Box } from "@material-ui/core"
import DarkModeToggler from "./DarkModeToggler"
type Props = {
  palette: "light" | "dark"
}

export const DarkModeTogglerExample = ({ palette = "light" }: Props): JSX.Element => {
  const [themeMode, setThemeMode] = useState(palette)
  const handleClick = () => setThemeMode(themeMode === "light" ? "dark" : "light")

  return (
    <Box
      marginBottom={2}
      display="flex"
      justifyContent="space-between"
      alignItems="center"
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
    >
      <DarkModeToggler themeMode={themeMode} onClick={() => handleClick()} />
    </Box>
  )
}

export default DarkModeTogglerExample

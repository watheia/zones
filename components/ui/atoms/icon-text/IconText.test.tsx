import * as React from "react"
import { render } from "@testing-library/react"
import IconText from "./IconText"

describe("ui.atoms.IconText", () => {
  it("should be rendered correctly", () => {
    const { asFragment } = render(<IconText fontIconClass="email" title="Email" />)
    expect(asFragment()).toMatchSnapshot()
  })
})

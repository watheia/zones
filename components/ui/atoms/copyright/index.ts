import { defaultProps, Copyright, CopyrightProps } from "./Copyright"

export { defaultProps, Copyright, CopyrightProps }
export default Copyright

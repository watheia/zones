import React from "react"
import { Box, Grid, Typography } from "@material-ui/core"
import CardProduct from "."
import SwiperImage from "@watheia/com.ui.molecules.swiper-image"

const item = {
  images: [
    {
      src: "https://cdn.watheia.org/assets/photos/coworking/place.jpg",
      alt: "",
    },
    {
      src: "https://cdn.watheia.org/assets/photos/coworking/place1.jpg",
      alt: "",
    },
    {
      src: "https://cdn.watheia.org/assets/photos/coworking/place2.jpg",
      alt: "",
    },
  ],
  title: "Tenoha Space",
  address: "Via E. Gola 4, 20147 Milan, Italy",
  price: "$980 / month",
}

export function Example1() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardProduct
        style={{ maxWidth: 500 }}
        mediaContent={
          <>
            <SwiperImage items={item.images} />
            <div
              style={{
                position: "absolute",
                left: 8,
                bottom: 16,
                zIndex: 9,
                background: "white",
                padding: "4px 8px",
                borderRadius: "8px",
              }}
            >
              <Typography variant="body1" color="primary">
                {item.price}
              </Typography>
            </div>
          </>
        }
        cardContent={
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Typography variant="h6" color="textPrimary" align="left">
                {item.title}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="body1" color="textSecondary" align="left">
                {item.address}
              </Typography>
            </Grid>
          </Grid>
        }
      />
    </Box>
  )
}

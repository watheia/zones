import React from "react"
import { Box, Grid, Typography } from "@material-ui/core"
import HeroSideImage from "."

export function Example1(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroSideImage imageSrc="https://cdn.watheia.org/assets/images/tech/thisisengineering-raeng-0l2PPgrHXX4-unsplash.jpg">
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h4" style={{ color: "inherit" }}>
              Join the world's leading companies at Watheia 2020
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" style={{ color: "inherit" }}>
              Download our overview and a member of our specialist team will be in touch.
            </Typography>
          </Grid>
        </Grid>
      </HeroSideImage>
    </Box>
  )
}

export function Example2(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroSideImage
        imageSrc="https://cdn.watheia.org/assets/images/tech/thisisengineering-raeng-1oYSrlQrpY4-unsplash.jpg"
        reverse
      >
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h4">
              Join the world's leading companies at Watheia 2020
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6">
              Download our overview and a member of our specialist team will be in touch.
            </Typography>
          </Grid>
        </Grid>
      </HeroSideImage>
    </Box>
  )
}

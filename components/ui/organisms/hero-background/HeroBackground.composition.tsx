import React from "react"
import { Box, Grid, Typography, Button, colors } from "@material-ui/core"
import HeroBackground from "."

export function Example1(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroBackground>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h3" style={{ color: "white", fontWeight: 900 }}>
              Join the world's leading companies at Watheia 2020
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="h5" style={{ color: "white" }} gutterBottom>
              Whether it’s Porsche, Stripe, Intercom, Amazon, or Google, something about
              Watheia works for our global partners.
            </Typography>
            <Typography variant="h5" style={{ color: "white" }}>
              Want more information? Download our overview and a member of our specialist
              team will be in touch to talk about your goals for Watheia 2020.
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Button variant="contained">Download exhibitor overview</Button>
          </Grid>
        </Grid>
      </HeroBackground>
    </Box>
  )
}

export function Example2(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroBackground backgroundColor={colors.blue[500]}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h3" style={{ color: "white", fontWeight: 900 }}>
              Join the world's leading companies at Watheia 2020
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="h5" style={{ color: "white" }} gutterBottom>
              Whether it’s Porsche, Stripe, Intercom, Amazon, or Google, something about
              Watheia works for our global partners.
            </Typography>
            <Typography variant="h5" style={{ color: "white" }}>
              Want more information? Download our overview and a member of our specialist
              team will be in touch to talk about your goals for Watheia 2020.
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Button variant="contained">Download exhibitor overview</Button>
          </Grid>
        </Grid>
      </HeroBackground>
    </Box>
  )
}

export function Example3(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroBackground backgroundImg="https://cdn.watheia.org/assets/images/tech/karim-ghantous-dxS2okXd-zo-unsplash.jpg">
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Typography variant="h3" style={{ color: "white", fontWeight: 900 }}>
              Join the world's leading companies at Watheia 2020
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Typography variant="h5" style={{ color: "white" }} gutterBottom>
              Whether it’s Porsche, Stripe, Intercom, Amazon, or Google, something about
              Watheia works for our global partners.
            </Typography>
            <Typography variant="h5" style={{ color: "white" }}>
              Want more information? Download our overview and a member of our specialist
              team will be in touch to talk about your goals for Watheia 2020.
            </Typography>
          </Grid>
          <Grid item xs={12} md={6}>
            <Button variant="contained">Download exhibitor overview</Button>
          </Grid>
        </Grid>
      </HeroBackground>
    </Box>
  )
}

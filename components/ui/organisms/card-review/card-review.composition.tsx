import React from "react"
import { Box, colors } from "@material-ui/core"
import CardReview from "."
import IconAlternate from "@watheia/com.ui.atoms.icon-alternate"

export function Example1() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardReview
        icon={
          <IconAlternate
            fontIconClass="fas fa-quote-right"
            size="medium"
            color={colors.indigo}
          />
        }
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        authorName="Veronica Adams"
        authorTitle="Growth Marketer, Crealytics"
        authorPhoto={{
          src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
          srcSet: "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg",
        }}
      />
    </Box>
  )
}

export function Example2() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardReview
        withShadow
        icon={
          <IconAlternate
            fontIconClass="fas fa-quote-right"
            size="medium"
            color={colors.indigo}
          />
        }
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        authorName="Veronica Adams"
        authorTitle="Growth Marketer, Crealytics"
        authorPhoto={{
          src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
          srcSet: "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg",
        }}
      />
    </Box>
  )
}

export function Example3() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardReview
        liftUp
        withShadow
        icon={
          <IconAlternate
            fontIconClass="fas fa-quote-right"
            size="medium"
            color={colors.indigo}
          />
        }
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        authorName="Veronica Adams"
        authorTitle="Growth Marketer, Crealytics"
        authorPhoto={{
          src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
          srcSet: "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg",
        }}
      />
    </Box>
  )
}

export function Example4() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardReview
        noBorder
        noShadow
        icon={
          <IconAlternate
            fontIconClass="fas fa-quote-right"
            size="medium"
            color={colors.indigo}
          />
        }
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        authorName="Veronica Adams"
        authorTitle="Growth Marketer, Crealytics"
        authorPhoto={{
          src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
          srcSet: "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg",
        }}
      />
    </Box>
  )
}

export function Example5() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <CardReview
        variant="outlined"
        icon={
          <IconAlternate
            fontIconClass="fas fa-quote-right"
            size="medium"
            color={colors.indigo}
          />
        }
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        authorName="Veronica Adams"
        authorTitle="Growth Marketer, Crealytics"
        authorPhoto={{
          src: "https://cdn.watheia.org/assets/photos/people/veronica-adams.jpg",
          srcSet: "https://cdn.watheia.org/assets/photos/people/veronica-adams@2x.jpg",
        }}
      />
    </Box>
  )
}

import NavigationProps from "./NavigationProps"

export default interface PageNavProps {
  className?: string
  onClose: () => void
  pages: NavigationProps
}

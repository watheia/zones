import React from "react"
import { Box, Button } from "@material-ui/core"
import Image from "@watheia/com.ui.atoms.image"
import SectionHeader from "@watheia/com.ui.molecules.section-header"
import HeroShaped from "."

export function Example(): JSX.Element {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroShaped
        leftSide={
          <SectionHeader
            title="Coworking made easy"
            subtitle="For entrepreneurs, startups and freelancers. Discover coworking spaces designed to inspire and to connect you to a community of motivated people."
            ctaGroup={[
              <Button variant="contained" color="primary">
                Book
              </Button>,
              <Button variant="outlined" color="primary">
                Browse
              </Button>,
            ]}
            align="left"
          />
        }
        rightSide={
          <Image
            src="https://cdn.watheia.org/assets/images/tech/joel-filipe-Lw7BruqPnJY-unsplash.jpg"
            alt="..."
            style={{ objectFit: "cover" }}
            lazy={false}
          />
        }
      />
    </Box>
  )
}

import React from "react"
import { Box, Typography } from "@material-ui/core"
import Parallax from "."
import SectionHeader from "@watheia/com.ui.molecules.section-header"
import CardBase from "@watheia/com.ui.organisms.card-base"

export default function Example() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <Parallax backgroundImage="https://cdn.watheia.org/assets/photos/blog/cover2.jpg">
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#007b3d3d",
          }}
        >
          <CardBase
            withShadow
            style={{ width: 500, height: "auto", background: "transparent" }}
          >
            <SectionHeader
              title={
                <span style={{ color: "white" }}>
                  Use flexible components.
                  <br />
                  <Typography component="span" variant="inherit" color="primary">
                    to build an app quickly.
                  </Typography>
                </span>
              }
              subtitle={
                <span style={{ color: "white" }}>
                  Watheia styles and extends Material UI components, but also included brand
                  new landing page focused components.
                </span>
              }
            />
          </CardBase>
        </div>
      </Parallax>
    </Box>
  )
}

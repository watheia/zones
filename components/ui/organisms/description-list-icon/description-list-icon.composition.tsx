import React from "react"
import { Box, colors } from "@material-ui/core"
import DescriptionListIcon from "."
import IconAlternate from "@watheia/com.ui.atoms.icon-alternate"
import Icon from "@watheia/com.ui.atoms.icon"

export function Example1() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <DescriptionListIcon
        title="Coworking communities"
        subtitle="Connect in spaces designed to bring incredible people together. Learn with them and take your project to new heights."
        icon={
          <Icon
            fontIconClass="far fa-address-book"
            size="large"
            fontIconColor={colors.purple[500]}
          />
        }
      />
    </Box>
  )
}

export function Example2() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <DescriptionListIcon
        title="Coworking communities"
        subtitle="Connect in spaces designed to bring incredible people together. Learn with them and take your project to new heights."
        icon={
          <IconAlternate
            fontIconClass="far fa-address-book"
            size="medium"
            color={colors.purple}
          />
        }
      />
    </Box>
  )
}

export function Example3() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <DescriptionListIcon
        title="Coworking communities"
        subtitle="Connect in spaces designed to bring incredible people together. Learn with them and take your project to new heights."
        icon={
          <IconAlternate
            fontIconClass="far fa-address-book"
            size="medium"
            color={colors.blue}
          />
        }
        align="left"
      />
    </Box>
  )
}

export default function Example5() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <DescriptionListIcon
        title="Coworking communities"
        subtitle="Connect in spaces designed to bring incredible people together. Learn with them and take your project to new heights."
        icon={
          <IconAlternate
            fontIconClass="far fa-address-book"
            size="medium"
            color={colors.blue}
          />
        }
        align="right"
      />
    </Box>
  )
}

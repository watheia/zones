import * as React from "react"
import { render } from "@testing-library/react"
import CountUpNumber from "."

describe("ui.atoms.CountUpNumber", () => {
  it("should be rendered correctly", () => {
    const { asFragment } = render(<CountUpNumber label="LABEL" start={1} end={2} />)
    expect(asFragment()).toMatchSnapshot()
  })
})

import { CountUpNumber, CountUpNumberProps } from "./count-up-number"

export { CountUpNumber, CountUpNumberProps }
export default CountUpNumber

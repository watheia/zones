export interface NumberProps {
  number: string | number
  title: string
}

export interface SwiperNumberProps {
  /**
   * External classes
   */
  className?: string
  /**
   * The array of numbers Record<string, any> which should consist of number and title
   */
  items: Array<NumberProps>
  /**
   * The additional properties to pass to the number Typography component
   */
  numberProps?: Record<string, any>
  /**
   * The additional properties to pass to the label Typography component
   */
  labelProps?: Record<string, any>
  // All other props
  [x: string]: any
}

import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import { CssBaseline, Paper } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
}))

type Props = Record<string, unknown>
export const Surface: React.FC<Props> = ({ children }): JSX.Element => {
  const classes = useStyles()
  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.layout}>
        <Paper elevation={1} className={classes.paper}>
          {children}
        </Paper>
      </main>
    </React.Fragment>
  )
}

export default Surface

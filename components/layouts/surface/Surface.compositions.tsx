import { Link, Typography } from "@material-ui/core"
import * as React from "react"
import Layout from "."

export function Example1() {
  return <Layout>{null}</Layout>
}

export function Example2() {
  return (
    <Layout>
      {" "}
      <Typography variant="body2" color="textSecondary" align="center">
        {"Copyright © "}
        <Link color="inherit" href="https://www.watheialabs.com">
          Watheia Labs, LLC
        </Link>{" "}
        {new Date().getFullYear()}.
      </Typography>
    </Layout>
  )
}

import React from "react"
import { render } from "@testing-library/react"
import Layout from "."
import { Typography } from "@material-ui/core"

describe("layouts/Surface", () => {
  it("SHOULD render without crashing", () => {
    render(
      <Layout>
        {" "}
        <Typography variant="h5" gutterBottom>
          Thank you for your order.
        </Typography>
        <Typography variant="subtitle1">
          Your order number is #2001539. We have emailed your order confirmation, and will
          send you an update when your order has shipped.
        </Typography>
      </Layout>,
    )
  })
})
